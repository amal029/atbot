#!/usr/bin/env python

import argparse
from selenium import webdriver
import time
import os
# import httplib


def login(driver=None, username='', password=''):
    u_element = driver.find_element_by_xpath(
        '//input[@placeholder="Email Address"]')
    p_element = driver.find_element_by_xpath('//input[@type="password"]')
    s_button = driver.find_element_by_xpath('//span[@id="submitButton"]')
    u_element.send_keys(username)
    p_element.send_keys(password)
    s_button.click()


def ValConvert(val):
    if type(val).__name__ == 'unicode':
        return val.encode('ascii', 'ignore')
    elif type(val).__name__ == 'str':
        return val
    else:
        return str(val)


def getInfo(url='', driver=None, email=''):
    try:
        driver.get(url)
        time.sleep(2)
        element = driver.find_element_by_xpath('//div[@id="default-hop-balance-remaining"]')
        remaining = element.text.strip().split('$')[1].strip()
        #remaining = element.text.split('\n')[1].strip().split('$')[1].strip()
        if float(remaining) < 20:
            t1 = 'echo "Subject: Top-up only have $ '
            t2 = ' left" | msmtp ' + email
            tosend = t1 + str(remaining) + t2
            os.system(tosend)
    finally:
        if driver is not None:
            driver.quit()


def main(username='', password='', fn=''):
    urls = ['https://federation.aucklandtransport.govt.nz/adfs/ls/?wa=wsignin1.0&wtrealm=https://at.govt.nz&wctx=https://at.govt.nz&wreply=https://at.govt.nz/myat',
            'https://at.govt.nz/myat']
    # urls = ['https://federation.aucklandtransport.govt.nz/adfs/ls/?wa=wsignin1.0&wtrealm=https://at.govt.nz&wctx=https://at.govt.nz&wreply=https://at.govt.nz/myat',
    #         'https://at.govt.nz/myat']
    driver = webdriver.Chrome()  # The web driver
    # driver = webdriver.Firefox()  # The web driver
    driver.get(urls[0])  # Get the login webpage
    # The state is now different and logged in
    login(driver, username, password)
    # Get the information from the table
    getInfo(urls[1], driver, fn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("username")
    parser.add_argument("password")
    parser.add_argument("email")
    args = parser.parse_args()
    main(args.username, args.password, args.email)
